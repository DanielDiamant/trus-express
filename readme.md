# Trus-Express

Express adapter for Trus validation.

## Installation
```
npm install --save trus-express
```

(!) Make sure to have [Trus][trus] installed

  [trus]: https://www.npmjs.com/package/trus

## Usage
```javascript
const { condition } = require("trus").methods;
const validate = require("trus-express").validate;

app.get( "/allowAll/:a",
    validate(
        condition( req => req.params.a == 1 )
    ),
    (req,res) => res.send("ok")
);
```


## Usage for permission
```javascript
var admin = condition( req => req.cookies.isAdmin ); // Check however you want

app.get( "/allowAll/:a", validate( admin ), (req,res) => res.send("ok") );
```
