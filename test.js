require('should');
require("should-sinon");
const trusExpress = require('./index.js');
const sinon = require('sinon');
const { any, none } = require('trus').methods;

describe("trus-express", () => {
  it("has a .validate() function", () => trusExpress.validate.should.be.a.Function());
  describe(".validate()", () => {
    it("resolves in positive validation", async () => {
      var validate = trusExpress.validate( any );
      var next = sinon.spy();
      var res = { statusCode: 200 };
      await validate( {}, res, next );
      next.should.be.calledWith();
      res.statusCode.should.be.exactly(200);
    })
    it("rejects in negative validation", async () => {
      var validate = trusExpress.validate( none );
      var next = sinon.spy();
      var res = { status: sinon.spy() };
      await validate( {}, res, next );
      next.should.be.called();
      res.status.should.be.calledWith(403);
    })
  })
})
