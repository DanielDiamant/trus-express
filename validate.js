module.exports = module.exports = method =>
  (request, response, next) =>
    method(request)
      .then( () => next() )
      .catch( e => {
        response.status(403);
        next(e || "Forbidden");
      } )
